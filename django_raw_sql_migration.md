# Django Raw SQL Migration with State Operations
Suppose that you want to create a custom table in a django migration with raw SQL. You can't just put the SQL in a `.RunSQL()` call because the change won't be reflected in the django model state. Luckily Django has a workaround for this. You can pass in a `state_operations` param:
```python
migrations.RunSQL(
    "ALTER TABLE musician ADD COLUMN name varchar(255) NOT NULL;",
    state_operations=[
        migrations.AddField(
            'musician', 'name', models.CharField(max_length=255),
        ),
    ],
)
```
This code snippet was taken straight from the [django docs](https://docs.djangoproject.com/en/3.1/ref/migration-operations/#runsql)
