# Useful links wiki

A personal collection of links from around the web that have useful information.

## Python Standard Lib Snippets ##

* [How to read csv inside of a zip file](https://stackoverflow.com/questions/26942476/reading-csv-zipped-files-in-python) 

## Django Related Python Snippets ##

* [How to do a batched query with a named cursor from inside Django ORM](https://stackoverflow.com/questions/30510593/how-can-i-use-server-side-cursors-with-django-and-psycopg2)
* [How to get authorization token for django DRF API](authtoken.md)
* [How to make a RunSQL migration with a django state change](django_raw_sql_migration.md)

## PostgreSQL Specific ##

* [Postgres functions for observing different part of postgres](https://pgstats.dev)

## General Database ##

* [Write a SQL DBMS from scratch](https://notes.eatonphil.com/database-basics.html)

## Git Related ##

* [How to manually squash commits](https://github.com/wprig/wprig/wiki/How-to-squash-commits)
* [How to bulk delete branches](git_bulk_delete.md)
