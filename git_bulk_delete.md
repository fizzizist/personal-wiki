# How to delete git branches in bulk
Sometimes after weeks of development and reviewing of other people's branches, you are left with a bunch of local branches that you don't need. Here is a quick command for deleting everything other than master.

Note: If you have another branch checked out other than master, this command will keep that one as well.

```bash
git branch | grep -v "master" | xargs git branch -D
```

If you want to *just* delete those that have already been merged:
```bash
git branch --merged | grep -v master | xargs git branch -D
```
