# How to find the API authorization token
For when you want to test the API using curl, here is how you get the auth token to do so
1. Open `shell_plus`
2. Run the following code:
```python
from rest_framework.authtoken.models import Token
Token.objects.first().key
```
3. Now you can use that token in a curl request like:
```bash
curl -X GET <some url> -H 'Authorization: Token <above token>'
```
